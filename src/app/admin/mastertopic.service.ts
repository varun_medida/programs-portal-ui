import { Injectable } from '@angular/core';
import { MasterTopic } from '../model/MasterTopic';
import { HttpClient } from '@angular/common/http';
import { ApphttpclientService } from '../apphttpclient.service';


@Injectable({
  providedIn: 'root'
})
export class MastertopicService {

  constructor(private http: HttpClient, private client: ApphttpclientService) { }

  addTopic(masterTopic: MasterTopic) {
    return this.client.post("programs/topics/", masterTopic);
  }

  getTopics() {
    console.log("get topics called");
    return this.client.get("programs/topics/");
  }

  deleteTopic(topicId: string) {
    return this.client.delete("programs/topics/" + topicId);
  }

  updateTopic(masterTopic: MasterTopic) {
    return this.client.put("programs/topics/" + masterTopic.topicId, masterTopic);
  }

}
