import { Component, OnInit } from '@angular/core';
import { AdminReportsService } from '../admin-reports.service';
declare var $:any;
@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {

  selectedBatch:any;
  batches:any;
  students:any;
  display:boolean=false;
  
  constructor(private adminReportsService:AdminReportsService) { }

  ngOnInit(): void {
      this.adminReportsService.getBatches().subscribe(res => {this.batches=res;
      })
  }
  selectChangeHandler (event: any) {
    //update the ui
    this.selectedBatch = event.target.value;
    this.display=false;
    this.adminReportsService.fetchReport(this.selectedBatch).subscribe(res => {
      this.students=res;
      this.display=true;
    $(document).ready(function() {
      $('#dataTable').DataTable();
    });
    });
  }
  }
