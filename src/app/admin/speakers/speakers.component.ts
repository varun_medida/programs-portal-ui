import { Component, OnInit } from '@angular/core';
import { Speaker } from 'src/app/model/speaker';
import {SpeakersService} from './speakers.service';
declare var $:any;

@Component({
  selector: 'app-speakers',
  templateUrl: './speakers.component.html',
  styleUrls: ['./speakers.component.css']
})
export class SpeakersComponent implements OnInit {

  speakerColumns = [ "Speaker ID", "Speaker Name", "Speaker Email", "Speaker Profile", "ACTION"];
  speakers : Speaker[] = [];
  speaker:Speaker;
  selectedSpeaker : Speaker ;
  isEdit : boolean = false;
  dialogTitle : string ;
  showMsg: boolean = false;
  allSpeakers:any;


  constructor(private speakerService : SpeakersService) {
    this.selectedSpeaker = new Speaker();
   }


  ngOnInit() {
    this.getSpeakers();
  }

  getSpeakers(){
    this.speakerService.getSpeakers(false).subscribe(res => {
      this.speakers = res; 
      $(document).ready(function() {
        $('#dataTable').DataTable();
      });
    });
  }

  deleteSpeaker(speaker : Speaker){
    this.speakerService.deleteSpeaker(speaker.speakerId).subscribe(res=>{
      console.log("speaker deleted");
      this.speakerService.getSpeakers(true);
    });
  }

  createSpeaker(){
    console.log("create speaker");
    this.selectedSpeaker = new Speaker();
    this.isEdit = false;
    this.dialogTitle = "Add Speaker";
  }

  updateSpeaker(speaker : Speaker){
    console.log("update speaker");
    this.selectedSpeaker = speaker;
    this.isEdit = true;
    this.dialogTitle = "Edit Speaker";
  }

  saveSpeakerAck(event){
    document.getElementById("closePopup").click();
    console.log("--"+event);
    this.showMsg=true;
    this.FadeOutMessage();
  }
  
  FadeOutMessage() {
    setTimeout( () => {
          this.showMsg = false;
        }, 2000);
   }
}
