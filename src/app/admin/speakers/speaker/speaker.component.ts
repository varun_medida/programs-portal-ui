import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Speaker } from 'src/app/model/speaker';
import {SpeakersService} from 'src/app/admin/speakers/speakers.service';

@Component({
  selector: 'app-speaker',
  templateUrl: './speaker.component.html',
  styleUrls: ['./speaker.component.css']
})
export class SpeakerComponent implements OnInit {

  @Input()
  isEdit : boolean = false;

  @Input()
  speaker : Speaker;

  @Output()
  saveEvent = new EventEmitter<any>();

  constructor(
    private speakerService : SpeakersService) {
   }
  
  ngOnInit() {
  }

  saveSpeaker(){
    if(this.isEdit){
      console.log("Edit Speaker");
      this.speakerService.updateSpeaker(this.speaker.speakerId, this.speaker).subscribe(res => {
        console.log("speaker updated");
        this.saveEvent.emit("success");
        this.speakerService.getSpeakers(true);
      });
    }
    else {
      console.log("Create Speaker");
      this.speakerService.addSpeaker(this.speaker).subscribe(res => {
        console.log("Speaker created");
        this.saveEvent.emit("success");
        this.speakerService.getSpeakers(true);
      });
    }
  }
}
