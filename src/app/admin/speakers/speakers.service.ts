import { Injectable } from '@angular/core';
import { ApphttpclientService } from '../../apphttpclient.service';
import { Speaker } from '../../model/speaker';
import { BehaviorSubject } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class SpeakersService {

  speakers : Speaker[] = [];
  private speakerDataSubject = new BehaviorSubject<Speaker[]>([]);

  constructor(private httpClientService : ApphttpclientService) { 
    this.initSpeakersData();
  }

  initSpeakersData(){
    this.getSpeakers(true);
  }

  getSpeakers(flag : boolean){
    if(flag || !this.speakerDataSubject.getValue()){
      this.httpClientService.get('programs/speakers').subscribe((res:Speaker[]) => {
          this.speakerDataSubject.next(res);
    });
   }
    return this.speakerDataSubject.asObservable();
  }

  deleteSpeaker(speakerId:number){
    return this.httpClientService.delete("programs/speaker/"+speakerId);
  }

  addSpeaker(speakerModel: any){
    return this.httpClientService.post("programs/speaker", speakerModel);
  }

  updateSpeaker(speakerId: number, speakerModel: any){
    return this.httpClientService.put("programs/speaker/"+speakerId, speakerModel);
  } 
}
