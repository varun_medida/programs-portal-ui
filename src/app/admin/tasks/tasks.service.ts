import { Injectable } from '@angular/core';
import { Task } from '../../model/task';
import { ApphttpclientService } from '../../apphttpclient.service';
import { BehaviorSubject } from 'rxjs';
import { Topic } from '../../model/topic';

@Injectable({
  providedIn: 'root'
})
export class TasksService {

  tasks : Task[] = [];
  private taskDataSubject = new BehaviorSubject<Task[]>([]);
  private topicDataSubject= new BehaviorSubject<Topic[]>([]);

  constructor(private httpClientService : ApphttpclientService) { 
  }

  getAllTopics(){
    return this.httpClientService.get("programs/topics")
  }

  deleteTask(taskId:number){
    return this.httpClientService.delete("programs/tasks/"+taskId);
  }

  addTask(topicId:number, taskModel: any){
    return this.httpClientService.post("programs/topic/"+topicId+"/task", taskModel);
  }

  updateTask(taskId: number, taskModel: any){
    return this.httpClientService.put("programs/tasks/"+taskId, taskModel);
  }

  getTopicById(selectedTopic: number) {
    return this.httpClientService.get("programs/topics/"+selectedTopic);
  }

  getTopicWithName(value: any) {
    this.httpClientService.get("programs/topics/title/"+value).subscribe((res:Topic[]) => {
      this.topicDataSubject.next(res);
    });
    return this.topicDataSubject.asObservable();
  }

  getTasksByTopic(selectedTopic: number) {
    return this.httpClientService.get("programs/topic/"+selectedTopic+"/tasks");
  }
}
