import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/model/task';
import { Topic } from 'src/app/model/topic';
import { TasksService } from './tasks.service'
declare var $:any;

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {

  taskColumns = [ "Task ID", "Task Status", "Task Title","Topic Name", "Task Description", "ACTION"];
  tasks : any;
  task:Task;
  selectedTask : Task ;
  isEdit : boolean = false;
  dialogTitle : string ;
  showMsg: boolean = false;
  display:boolean=false;
  topics:Topic[];
  error:any;
  selectedTopic:Topic;
  topicName:any;
  topicId:any;

  constructor(private taskService : TasksService) {
    this.selectedTask = new Task();
   }


  ngOnInit() {
    this.taskService.getAllTopics().subscribe((res:Topic[]) => {this.topics=res;
    })
  }

  selectChangeHandler (event: any) {
    this.display=false;
    this.taskService.getTopicById(event.target.value).subscribe((res: Topic)=>{
      this.selectedTopic=res;
      this.display=true;
    })
    console.log("value is "+event.target.value);
    this.topicId=event.target.value;
    this.getTasksByTopic(this.topicId);
  }

  private getTasksByTopic(topicId: number) {
    this.taskService.getTasksByTopic(topicId).subscribe(res => {
      this.tasks = res;
      this.display = true;
      $(document).ready(function () {
        $('#dataTable').DataTable();
      });
    }, err => {
      this.error = err;
      this.display = false;
    });
  }

  deleteTask(task : Task){
    this.taskService.deleteTask(task.taskId).subscribe(res=>{
      console.log("Task deleted");
      this.getTasksByTopic(this.topicId);
    });
  }

  createTask(){
    console.log("create task");
    this.selectedTask = new Task();
    this.topicName="";
    this.isEdit = false;
    this.dialogTitle = "Add Task";
  }

  updateTask(task : Task){
    this.topicName=this.selectedTopic.topicTitle;
    console.log("update task");
    this.selectedTask = task;
    this.isEdit = true;
    this.dialogTitle = "Edit Task";
  }

  saveTaskAck(event){
    document.getElementById("closePopup").click();
    console.log("--"+event);
    this.showMsg=true;
    this.FadeOutMessage();
  }
  
  FadeOutMessage() {
    setTimeout( () => {
          this.showMsg = false;
        }, 2000);
   }

}
