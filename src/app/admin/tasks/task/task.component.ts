import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Task } from 'src/app/model/task';
import { TasksService } from 'src/app/admin/tasks/tasks.service'
import { Topic } from 'src/app/model/topic';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  topics:Topic[];
  selectedTopic;


  @Input()
  isEdit : boolean = false;

  @Input()
  task : Task;

  @Input()
  topic: String;

  topicName:String=this.topic;

  @Output()
  saveEvent = new EventEmitter<any>();

  constructor(private taskService : TasksService) {
   }
  
  ngOnInit() {
  }

  getTopics(event:any){
    console.log("entered value "+event.target.value);
    this.topicName=event.target.value;
    this.getTopicsWithName(this.topicName);
  }

  private getTopicsWithName(topic: any) {
    this.taskService.getTopicWithName(topic).subscribe(res => {
      console.log(res);
      this.topics = res;
    });
  }

  saveTask(){
    if(this.isEdit){
      this.getTopicsWithName(this.topic);
      this.selectedTopic= this.topics[0];
      console.log("Edit Task");
        this.task.topicId=this.selectedTopic.topicId;
        this.taskService.updateTask(this.task.taskId, this.task).subscribe(res => {
        console.log("Task updated "+this.task);
        this.saveEvent.emit("success");
        this.taskService.getTasksByTopic(this.selectedTopic.topicId);
        console.log("updated and returned")
      });
    }
    else {
      console.log("Create Task");
      this.selectedTopic= this.topics[0];
      console.log(this.task);
      this.taskService.addTask(this.selectedTopic.topicId, this.task).subscribe(res => {
        console.log("Task created");
        this.saveEvent.emit("success");
        this.taskService.getTasksByTopic(1);
      });
    }
  }
}
