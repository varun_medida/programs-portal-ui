import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { ReportsComponent } from './reports/reports.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TopicsComponent } from './topics/topics.component';
import { TasksComponent } from "./tasks/tasks.component";
import { SpeakersComponent } from "./speakers/speakers.component";
import { MasterTopicComponent } from './master-topic/master-topic.component';
import { CollegeComponent } from './college/college.component';
import { BatchesComponent } from './batches/batches.component';


const routes: Routes = [
  {
    path: 'admin',
    component: HomePageComponent,
    children: [
      { path: 'reports', component: ReportsComponent },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'topics', component: TopicsComponent },
      { path: 'mastertopics', component: MasterTopicComponent },
      { path: 'speakers', component: SpeakersComponent },
      { path: 'tasks', component: TasksComponent },
      { path: 'mastertopics', component: MasterTopicComponent },
      { path: 'colleges', component: CollegeComponent },
      { path: 'batches', component: BatchesComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
