import { TestBed } from '@angular/core/testing';

import { MastertopicService } from './mastertopic.service';

describe('MastertopicService', () => {
  let service: MastertopicService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MastertopicService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
