import { Injectable } from '@angular/core';
import { ApphttpclientService } from '../apphttpclient.service';

@Injectable({
  providedIn: 'root'
})
export class AdminReportsService {
  
  constructor(private httpClientService:ApphttpclientService) { }

  getBatches(){
      return this.httpClientService.get("programs/batches");
  }

  fetchReport(batchId:number) {
   return this.httpClientService.get("programs/attendance/batches/"+batchId);
  }
}
