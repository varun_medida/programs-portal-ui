import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { College } from '../model/College';
import { ApphttpclientService } from '../apphttpclient.service';
@Injectable({
  providedIn: 'root'
})
export class CollegeService {

  constructor(private http: HttpClient, private client: ApphttpclientService) { }

  addCollege(college: College) {
    return this.client.post("programs/colleges", college);
  }

  getColleges() {
    return this.client.get("programs/colleges");
  }

  deleteCollege(collegeId: number) {
    return this.client.delete("programs/colleges/" + collegeId)
  }

  updateCollege(college: College) {
    return this.client.put("programs/colleges/" + college.collegeId, college);
  }

}
