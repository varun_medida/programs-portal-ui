import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminRoutingModule } from './admin-routing.module';
import { HomePageComponent } from './home-page/home-page.component';
import { SharedModule } from '../shared/shared.module';
import { ReportsComponent } from './reports/reports.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TopicsComponent } from './topics/topics.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule, OWL_DATE_TIME_FORMATS } from 'ng-pick-datetime';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TopicComponent } from './topics/topic/topic.component';
import { WebinarComponent } from './topics/webinar/webinar.component';
import { EmailComponent } from './topics/email/email.component';
import { MasterTopicComponent } from './master-topic/master-topic.component';
import { SpeakersComponent } from './speakers/speakers.component';
import { SpeakerComponent } from './speakers/speaker/speaker.component';
import { TasksComponent } from './tasks/tasks.component';
import { TaskComponent } from './tasks/task/task.component';
import { CollegeComponent } from './college/college.component';
import { BatchesComponent } from './batches/batches.component';
import { BatchComponent } from './batches/batch/batch.component';

export const MY_NATIVE_FORMATS = {
  fullPickerInput: { year: 'numeric', month: 'numeric', day: 'numeric', hour: 'numeric', minute: 'numeric' },
  datePickerInput: { year: 'numeric', month: 'numeric', day: 'numeric' },
  timePickerInput: { hour: 'numeric', minute: 'numeric' },
  monthYearLabel: { year: 'numeric', month: 'short' },
  dateA11yLabel: { year: 'numeric', month: 'long', day: 'numeric' },
  monthYearA11yLabel: { year: 'numeric', month: 'long' },
};

@NgModule({
  declarations: [HomePageComponent, ReportsComponent, DashboardComponent, TopicsComponent, TopicComponent, WebinarComponent, EmailComponent, MasterTopicComponent, SpeakersComponent, SpeakerComponent, TasksComponent, TaskComponent, CollegeComponent, BatchesComponent, BatchComponent],
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    AdminRoutingModule,
    SharedModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    FormsModule
  ],
  providers: [
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_NATIVE_FORMATS },
  ]
})
export class AdminModule { }
