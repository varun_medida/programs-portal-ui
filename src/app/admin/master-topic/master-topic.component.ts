import { Component, OnInit } from '@angular/core';
import { TopicsService } from '../topics.service';
import { MastertopicService } from '../mastertopic.service';
import { MasterTopic } from 'src/app/model/MasterTopic';
declare var $: any;
@Component({
  selector: 'app-master-topic',
  templateUrl: './master-topic.component.html',
  styleUrls: ['./master-topic.component.css']
})
export class MasterTopicComponent implements OnInit {

  constructor(private topicService: MastertopicService) { }

  public mastertopics: object;
  public msg: string;
  public masterTopic: MasterTopic = new MasterTopic();
  public isEdit: boolean = false;
  public flag: boolean = false;
  public delete:boolean=false;
  public topicToDelete:MasterTopic;

  ngOnInit(): void {
    this.showTopics();
  }

  public showTopics() {
    this.topicService.getTopics().subscribe(data => {
      console.log(this.mastertopics);
      this.mastertopics = data;
      let flag=this.flag;

      $(document).ready(function () {
        $('#masterTopicsTable').DataTable();
      });
      

      // 

    });
  }
  public deleteTopic(topicId:string) {
    console.log("sample topic called");
    console.log(topicId);
    this.topicService.deleteTopic(topicId).subscribe(data => {
      this.showTopics();
      this.msg = "Successfully Deleted  Topic";
      this.delete=false;
      $('#addTopicModal').modal('hide');
    });
  }

  public addTopic(topic: MasterTopic) {

    if (this.isEdit) {
      this.topicService.updateTopic(topic).subscribe(data => {
        console.log(data);
        this.msg = "Successfully Updated Topic";
        this.showTopics();
        $('#addTopicModal').modal('hide');
      });
    }
    else {
      this.topicService.addTopic(topic).subscribe(data => {
        this.msg = "Successfully added  Topic";
        this.showTopics();
        $('#addTopicModal').modal('hide');
      });
    }
  }

  public editTopic(topic: MasterTopic) {
    this.isEdit = true;
    this.delete=false;
    this.masterTopic.topicId = topic.topicId;
    this.masterTopic.topicTitle = topic.topicTitle;
    this.masterTopic.topicDescription = topic.topicDescription;
    $('#addTopicModal').modal('show');
    console.log(topic);
    console.log(this.masterTopic);

  }

  public addTopicBtnFunc() {
    console.log("add topic called");
    this.isEdit = false;
    this.delete=false;
    this.masterTopic.topicId = null;
    this.masterTopic.topicTitle = null;
    this.masterTopic.topicDescription = null;
  }

  public openDeleteModal(topic:MasterTopic){
    console.log("open delete topic modal called");
    this.delete=true;
    $('#addTopicModal').modal('show');
    this.topicToDelete=topic;
  }
}
