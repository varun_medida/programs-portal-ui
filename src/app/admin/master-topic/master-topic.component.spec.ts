import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterTopicComponent } from './master-topic.component';

describe('MasterTopicComponent', () => {
  let component: MasterTopicComponent;
  let fixture: ComponentFixture<MasterTopicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterTopicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterTopicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
