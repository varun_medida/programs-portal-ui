import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {MasterTopic} from "../../model/MasterTopic";
import { ApphttpclientService } from '../../apphttpclient.service';

@Injectable({
  providedIn: 'root'
})
export class MastertopicService {

  constructor(private http: HttpClient,private httpClientService:ApphttpclientService) { }

  addTopic(masterTopic: MasterTopic) {
    console.log("addtopics called");
    return this.httpClientService.post("programs/topics/",masterTopic);
  }

  getTopics() {
    console.log("get topics called");
    return this.httpClientService.get("programs/topics/")
  }

  deleteTopic(topicId: number) {
    return this.httpClientService.delete("programs/topics/"+topicId)
  }

  updateTopic(masterTopic: MasterTopic) {
    return this.httpClientService.put("programs/topics/"+masterTopic.topicId,masterTopic)
  }
}
