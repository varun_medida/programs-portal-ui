import { Component, OnInit } from '@angular/core';
import { College } from 'src/app/model/College';
import { CollegeService } from '../college.service';
declare var $: any;

@Component({
  selector: 'app-college',
  templateUrl: './college.component.html',
  styleUrls: ['./college.component.css']
})
export class CollegeComponent implements OnInit {

  constructor(private collegeServ: CollegeService) { }

  public msg: string;
  public colleges: object;
  public college: College = new College();
  public isEdit: boolean = false;
  public delete: boolean = false;
  public collegeToDelete: College;

  ngOnInit(): void {
    this.showColleges();
  }

  public showColleges() {
    console.log("show colleges called");
    this.collegeServ.getColleges().subscribe(data => {
      this.colleges = data;
      $(document).ready(function() {
        $('#masterCollegesTable').DataTable();
      });
    })

  }
  public deleteCollege(collegeId: number) {
    this.collegeServ.deleteCollege(collegeId).subscribe(data => {
      console.log(data);
      this.showColleges();
      this.delete=false;
      $('#addCollegeModal').modal('hide');
      this.msg = "College Successfully Deleted";
    })
  }
  public editCollege(college: College) {
    this.college.city = college.city;
    this.college.collegeId = college.collegeId;
    this.college.collegeName = college.collegeName;
    this.college.group = college.group;
    this.college.ptoEmail = college.ptoEmail;
    this.college.ptoMobile = college.ptoMobile;
    this.college.ptoName = college.ptoName;
    this.isEdit = true;
    this.delete = false;
    $('#addCollegeModal').modal('show');
  }

  public addCollege(college: College) {
    console.log("add college is called");
    if (this.isEdit) {
      this.collegeServ.updateCollege(college).subscribe(data => {
        console.log(data);
        this.msg = "Successfully Updated College";
        this.showColleges();
        $('#addCollegeModal').modal('hide');
      });
    }
    else {
      this.collegeServ.addCollege(college).subscribe(data => {
        this.msg = "Successfully added  College";
        this.showColleges();
        this.msg = "Successfully Added College";
        $('#addCollegeModal').modal('hide');
      });
    }
  }

  public addCollegeBtnFunc() {
    this.delete = false;
    this.college = new College();
    this.isEdit = false;
  }
  public openDeleteModal(college: College) {
    console.log("opnedeletemodal called");
    $('#addCollegeModal').modal('show');
    this.delete = true;
    this.collegeToDelete = college;
  }

}
