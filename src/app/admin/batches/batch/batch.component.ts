import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BatchModel } from 'src/app/model/batch-model';
import { ProgramModel } from 'src/app/model/program-model';
import { BatchesService } from '../batches.service';

@Component({
  selector: 'app-batch',
  templateUrl: './batch.component.html',
  styleUrls: ['./batch.component.css']
})
export class BatchComponent implements OnInit {

  @Input()
  create:boolean;
  @Input()
  update:boolean;
  @Input()
  delete:boolean;
  @Input()
  selectedBatch:BatchModel;
  @Input()
  selectedProgram:ProgramModel;
  @Output()
  batchEvent= new EventEmitter<any>();
  
  constructor(private batchService:BatchesService) { }

  ngOnInit(): void {
  }

  addBatch(){
    this.selectedBatch.programId=this.selectedProgram.id;
    console.log("Adding Batch----")
    this.selectedBatch.startDate=this.converttoDate(this.selectedBatch.startDate);
    this.selectedBatch.endDate=this.converttoDate(this.selectedBatch.endDate);
    this.batchService.addBatch(this.selectedBatch).subscribe(res=>{
      this.batchEvent.emit("success");
    })
  }

  updateBatch(){
    this.selectedBatch.programId=this.selectedProgram.id;
    this.selectedBatch.startDate=this.converttoDate(this.selectedBatch.startDate);
    this.selectedBatch.endDate=this.converttoDate(this.selectedBatch.endDate);
    console.log(this.selectedBatch);
    this.batchService.updateBatch(this.selectedBatch).subscribe(res=>{
      this.batchEvent.emit("edited");
    })

  }

  deleteBatch(){
    this.batchService.deleteBatch(this.selectedBatch).subscribe(res=>{
      this.batchEvent.emit("deleted");
    })
  }

  converttoDate(str) {
    var month, day, year, hours, minutes, seconds;
    var date = new Date(str);
        month = ("0" + (date.getMonth() + 1)).slice(-2);
        day = ("0" + date.getDate()).slice(-2);
    hours = ("0" + date.getHours()).slice(-2);
    minutes = ("0" + date.getMinutes()).slice(-2);
    seconds = ("0" + date.getSeconds()).slice(-2);

    var mySQLDate = [date.getFullYear(), month, day].join("-");
    var mySQLTime = [hours, minutes].join(":");
    return [mySQLDate, mySQLTime].join(" ");
}

}
