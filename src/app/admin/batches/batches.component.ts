import { Component, OnInit } from '@angular/core';
import { BatchesService } from './batches.service';
import { ProgramModel } from 'src/app/model/program-model';
import { BatchModel } from 'src/app/model/batch-model';
declare var $:any;

@Component({
  selector: 'app-batches',
  templateUrl: './batches.component.html',
  styleUrls: ['./batches.component.css']
})
export class BatchesComponent implements OnInit {

  programs: ProgramModel[];
  selectedProgram: ProgramModel=new ProgramModel();
  batches:BatchModel[];
  selectedBatch: BatchModel;
  display:boolean=false;
  create:boolean=false;
  update:boolean=false;
  delete:boolean=false;
  styleAlert:string;
  message:string;

  constructor(private batchesService:BatchesService) { }

  ngOnInit(): void {
    this.batchesService.getAllPrograms().subscribe((res:ProgramModel[])=>{
      this.programs=res;
    })
    if(this.selectedProgram.id!=undefined){
      this.getPrograms(this.selectedProgram.id);
    }
  }

  getPrograms(programId){
    this.batchesService.getBatchesByProgram(programId).subscribe((res:BatchModel[])=>{
      this.batches=res;
      this.display=true;
      $(document).ready(function() {
        $('#dataTable').DataTable();
      });
    })
  }

  selectChangeHandler(event){
    this.batchesService.getProgramById(event.target.value).subscribe((res:ProgramModel)=>{
     this.selectedProgram=res;
    })
    this.getPrograms(event.target.value)
    this.message=null;
  }

  createBatch(){
    this.create=true;
    this.update=false;
    this.delete=false;
    this.selectedBatch=new BatchModel();
  }

  updateBatch(batch){
    this.update=true;
    this.create=false;
    this.delete=false;
    this.selectedBatch=batch;
  }

  deleteBatch(batch){
    this.update=false;
    this.create=false;
    this.delete=true;
    this.selectedBatch=batch;
  }

  batchEventAck(value){
    if(value=="success"){
      this.styleAlert="alert alert-success"
      this.message="Batch created successfully"
    }
    if(value=="edited"){
      this.styleAlert="alert alert-success"
      this.message="Batch updated successfully"
    }
    if(value=="deleted"){
      this.styleAlert="alert alert-success"
      this.message="Batch deleted successfully"
    }
    document.getElementById("closeBatchPopup").click();
    this.ngOnInit();
  }

}
