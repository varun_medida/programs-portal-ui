import { Injectable } from '@angular/core';
import { ApphttpclientService } from 'src/app/apphttpclient.service';

@Injectable({
  providedIn: 'root'
})
export class BatchesService {
 
  constructor(private httpClient:ApphttpclientService) { }

  getAllPrograms() {
    return this.httpClient.get("programs");
  }

  getProgramById(programId: number) {
    return this.httpClient.get("programs/"+programId);
  }

  getBatchesByProgram(programId:number) {
    return this.httpClient.get("programs/batches?requestParams="+programId);
  }

  updateBatch(selectedBatch: import("../../model/batch-model").BatchModel) {
    return this.httpClient.put("programs/batches/"+selectedBatch.id,selectedBatch);
  }

  addBatch(selectedBatch: import("../../model/batch-model").BatchModel) {
    return this.httpClient.post("programs/batches/",selectedBatch);
  }

  deleteBatch(selectedBatch: import("../../model/batch-model").BatchModel) {
   return this.httpClient.delete("programs/batches/"+selectedBatch.id);
  }

}
