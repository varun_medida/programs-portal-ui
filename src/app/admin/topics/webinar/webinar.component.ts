import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BatchTopic } from 'src/app/model/batch-topics';
import { TopicsService } from '../../topics.service';
import { Batch } from 'src/app/model/batch';
import { Webinar } from 'src/app/model/webinar';
import { TimeZone } from 'src/app/model/timezone';

@Component({
  selector: 'app-webinar',
  templateUrl: './webinar.component.html',
  styleUrls: ['./webinar.component.css']
})
export class WebinarComponent implements OnInit {

  @Input()
  selectedBatchTopic:BatchTopic;
  @Input()
  selectedBatch:Batch;
  @Input()
  webinar:Webinar;
  timezones: TimeZone[];
  edit:boolean=false;
  @Input()
  displaydetails:boolean;
  @Output()
  webinarEvent = new EventEmitter<any>();

  constructor(private topicsService:TopicsService) { }

  ngOnInit(): void {
    this.topicsService.getTimezones().subscribe((res:TimeZone[])=>{
      this.timezones=res;
    })
    this.edit=false;
  }

  selectChangeHandler(event:any){
    this.webinar.timeZone=event.target.value;
  }

  createWebinar(){
    this.webinar.startTime=this.converttoDate(this.webinar.startTime);
    this.webinar.endTime=this.converttoDate(this.webinar.endTime);
    this.topicsService.createWebinar(this.selectedBatchTopic.id,this.webinar).subscribe(res=>{
      this.webinarEvent.emit("success");
    },err=>{this.webinarEvent.emit("failed")})
  }

  editWebinar(){
    this.edit=true;
  }

  cancelEditWebinar(){
    this.edit=false;
  }

  saveWebinar(){
    this.webinar.startTime=this.converttoDate(this.webinar.startTime);
    this.webinar.endTime=this.converttoDate(this.webinar.endTime);
    console.log(this.webinar);
    this.edit=false;
    this.topicsService.saveWebinar(this.selectedBatchTopic,this.webinar).subscribe(res=>{
      this.webinarEvent.emit("edited");
    })
  }

  converttoDate(str) {
    var month, day, year, hours, minutes, seconds;
    var date = new Date(str);
        month = ("0" + (date.getMonth() + 1)).slice(-2);
        day = ("0" + date.getDate()).slice(-2);
    hours = ("0" + date.getHours()).slice(-2);
    minutes = ("0" + date.getMinutes()).slice(-2);
    seconds = ("0" + date.getSeconds()).slice(-2);

    var mySQLDate = [date.getFullYear(), month, day].join("-");
    var mySQLTime = [hours, minutes].join(":");
    return [mySQLDate, mySQLTime].join(" ");
}


}
