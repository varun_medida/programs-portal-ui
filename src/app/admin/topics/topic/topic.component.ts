import { Component, OnInit, Input } from '@angular/core';
import { TopicsService } from '../../topics.service';
import { Batch } from 'src/app/model/batch';

declare var $:any;
@Component({
  selector: 'app-topic',
  templateUrl: './topic.component.html',
  styleUrls: ['./topic.component.css']
})
export class TopicComponent implements OnInit {

  topics:any;
  selectedTopic;
  selectedSpeaker;
  speakers;
  plannedDate;
  @Input()
  selectedBatch:Batch;
  batchTopicModel:any;
  constructor(private topicsService:TopicsService) { }

  ngOnInit(): void {
    this.getAllTopics();
    
  }

  getAllTopics(){
    this.topicsService.getAllTopics().subscribe(res=>{
      this.topics=res;
    })
  }
 

  getSpeakers(event:any){
    this.topicsService.getSpeakers(event.target.value).subscribe(
      res=>{
      this.speakers=res;
      }
    )
  }

  addTopicToBatch(){
    this.selectedSpeaker= this.speakers[0];
    console.log(this.plannedDate);
    console.log(this.selectedBatch);
    this.batchTopicModel={"batchId": this.selectedBatch.id, "topicId": this.selectedTopic.topicId,
     "speakerId": this.selectedSpeaker.speakerId,"plannedDate":this.plannedDate};
     console.log(this.batchTopicModel);
    this.topicsService.addTopicToBatch(this.batchTopicModel).subscribe(res=>{console.log(res)
    })
  }

//   converttoDate(str) {
//     var month, day, year, hours, minutes, seconds;
//     var date = new Date(str);
//         month = ("0" + (date.getMonth() + 1)).slice(-2);
//         day = ("0" + date.getDate()).slice(-2);
//     hours = ("0" + date.getHours()).slice(-2);
//     minutes = ("0" + date.getMinutes()).slice(-2);
//     seconds = ("0" + date.getSeconds()).slice(-2);

//     var mySQLDate = [date.getFullYear(), month, day].join("-");
//     var mySQLTime = [hours, minutes, seconds].join(":");
//     return [mySQLDate, mySQLTime].join(" ");
// }
}
