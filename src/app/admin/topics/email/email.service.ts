import { Injectable } from '@angular/core';
import {ApphttpclientService} from "../../../apphttpclient.service";

@Injectable({
  providedIn: 'root'
})
export class EmailService {
 

  constructor(private httpClientService:ApphttpclientService) { }

  getTemplates(){
    return this.httpClientService.getTemplate("notifications/templates");
  }

  getTemplateById(selectTemplateId: number){
    return this.httpClientService.getTemplate("notifications/templates/"+selectTemplateId);
  }

  sendEmail(emailSending: import("../../../model/email").Email) {
    return this.httpClientService.postEmail("notifications/emaildetails/",emailSending)
  }
  
}
