import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {EmailTemplate} from "../../../model/email-template";
import {EmailService} from "./email.service";
import {BatchTopic} from "../../../model/batch-topics";
import {Email} from "../../../model/email";
import {Batch} from "../../../model/batch";

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.css']
})
export class EmailComponent implements OnInit {

  @Input()
  selectedBatchTopic:BatchTopic;
  @Input()
  selectedBatch:Batch;
  @Input()
  emailSending: Email;

  @Output()
  emailEvent = new EventEmitter<any>();


  template: string;
  participationStatus: string;
  subject: string ="Select The Template First";
  templates:EmailTemplate[];


  constructor(private emailService:EmailService) { }

  ngOnInit(): void {
    this.emailService.getTemplates().subscribe((res:EmailTemplate[]) => {this.templates=res;
    })
    console.log(this.selectedBatchTopic);
    console.log(this.selectedBatchTopic!=undefined);
  }

  sendEmail()
  {
    this.emailSending.batchId=this.selectedBatch.id;
    this.emailSending.batchTopicId=this.selectedBatchTopic.topicModel.topicId;
    this.emailSending.participantStatus=this.participationStatus;
    this.emailSending.template=this.template;
    this.emailSending.emailType ="REGISTRATION_LINK";
    this.emailService.sendEmail(this.emailSending).subscribe(res=>{
    this.emailEvent.emit("success");
    })
  }


  onChange(event: any) {

   /* this.emailService.getTemplateById(event.target.value).subscribe((res: EmailTemplate)=>{
      this.subject=res.subject;
      this.template=res.templateName;
    })
*/
    this.template=event.target.value;
    console.log(this.template);
    this.subject="Demo Subject";
    console.log(this.selectedBatchTopic.topicModel.topicTitle);
  }



  onStatusChange(event: any) {
    this.participationStatus=event.target.value;
  }
}
