import { Component, OnInit } from '@angular/core';
import { TopicsService } from '../topics.service';
import { Batch } from 'src/app/model/batch';
import { BatchTopic } from 'src/app/model/batch-topics';
import { Webinar } from 'src/app/model/webinar';
import {Email} from "../../model/email";

declare var $:any;
@Component({
  selector: 'app-topics',
  templateUrl: './topics.component.html',
  styleUrls: ['./topics.component.css']
})
export class TopicsComponent implements OnInit {

  batches:Batch[];
  selectedBatch:Batch=new Batch();
  display:boolean=false;
  topics:any;
  error:any;
  selectedBatchTopic:BatchTopic;
  message:string;
  styleAlert:string;
  displaydetails:boolean;
  webinar:Webinar=new Webinar();
  emailSending: Email;
  constructor(private topicsService:TopicsService) { }

  ngOnInit(): void {
    this.topicsService.getBatches().subscribe((res:Batch[]) => {this.batches=res;
    })
    if(this.selectedBatch.id!=undefined){
      this.getTopics(this.selectedBatch.id);
    }
  }

  getTopics(value){
    this.topicsService.getTopicsByBatch(value).subscribe(res=>{
      this.topics=res;
      console.log(this.topics)
      this.display=true;
      $(document).ready(function() {
        $('#dataTable').DataTable();
      });
    },err=>{this.error=err;
    this.display=false;})
  }

  selectChangeHandler (event: any) {
    this.display=false;
    this.topicsService.getBatchById(event.target.value).subscribe((res: Batch)=>{
      this.selectedBatch=res;
    })
    this.message=null;
    this.getTopics(event.target.value)
  }

  createWebinar(topic){
    this.selectedBatchTopic=topic;
    this.displaydetails=false;
    this.webinar= new Webinar();
  }

  webinarComponentAck(value){
    if(value=="success"){
      this.styleAlert="alert alert-success"
      this.message="Webinar created successfully"
    }if(value=="failure"){
      this.styleAlert="alert alert-danger"
      this.message="Webinar could not be created";
    }if(value=="edited"){
      this.styleAlert="alert alert-success"
      this.message="Webinar edited successfully"
    }
    document.getElementById("closePopup").click();
  }

  emailComponentAck(value){
    if(value=="success"){
      this.styleAlert="alert alert-success"
      this.message="Email is triggered"
    }
    document.getElementById("closeEmailDialog").click();
    this.ngOnInit();
  }

  webinarDetails(topic){
    this.selectedBatchTopic=topic;
    this.topicsService.getWebinarDetails(this.selectedBatchTopic.webinarKey).subscribe((res:Webinar)=>{
      this.webinar=res;
    })
    this.displaydetails=true;
  }


  sendEmail(topic)
  {
    this.selectedBatchTopic=topic;
    this.displaydetails=false;
    this.emailSending = new Email();
  }


}
