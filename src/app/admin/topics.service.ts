import { Injectable } from '@angular/core';
import { ApphttpclientService } from '../apphttpclient.service';

@Injectable({
  providedIn: 'root'
})
export class TopicsService {


  constructor(private httpClientService: ApphttpclientService) { }

  getBatches() {
    return this.httpClientService.get("programs/batches");
  }

  getBatchById(selectedBatch: number) {
    return this.httpClientService.get("programs/batches/" + selectedBatch);
  }

  getTopicsByBatch(selectedBatch: number) {
    return this.httpClientService.get("programs/batch-topic?batchId=" + selectedBatch+"&sort=plannedDate,asc");
  }

  getAllTopics() {
    return this.httpClientService.get("programs/topics/")
  }

  getSpeakers(value: any) {
    return this.httpClientService.get("programs/speaker/name/" + value)
  }

  addTopicToBatch(batchTopicModel: any) {
    return this.httpClientService.post("programs/batch-topic", batchTopicModel)
  }

  getTimezones() {
    return this.httpClientService.get("programs/timezones")
  }

  createWebinar(batchId: number, webinar: import("../model/webinar").Webinar) {
    return this.httpClientService.postWebinar("gotowebinar/webinars/batchtopicid/" + batchId, webinar);
  }

  getWebinarDetails(webinarKey: string) {
    return this.httpClientService.getWebinar("gotowebinar/webinars/webinarkey/"+webinarKey);
  }

  saveWebinar(selectedBatchTopic: import("../model/batch-topics").BatchTopic, webinar: import("../model/webinar").Webinar) {
    return  this.httpClientService.putWebinar("gotowebinar/webinars/"+selectedBatchTopic.webinarKey+"/batchtopicid/"+selectedBatchTopic.id,webinar);
  }

}
