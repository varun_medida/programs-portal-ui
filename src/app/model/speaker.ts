export class Speaker{
    speakerId: number;

    speakerName: string;

    speakerProfile: string;

    speakerPicPath: string;

    speakerEmail: string;
}