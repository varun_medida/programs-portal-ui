export class Batch{
    id: number;
    batchCode: string;
    batchName: string;
    programId: number;
    startDate: any;
    endDate: any
}