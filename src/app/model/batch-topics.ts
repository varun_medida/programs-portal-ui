import { Speaker } from './speaker';
import { Topic } from './topic';

export class BatchTopic{
    id: number;

    topicId: number;

    speakerId:  number;

    batchId: number;

    plannedDate: Date

    startTime: Date;

    endTime:  Date;

    status: string

    webinarKey: string;

    webinarId: string

    sessionKey: string;

    registrationUrl: string;

    recordingLink: string;

    attendanceConsideration: number;

    speakerModel: Speaker;
    
    topicModel:Topic;
}