export class Webinar{
    subject:string;
    description:string;
    timeZone:string;
    registrationUrl:string;
    startTime:any;
    endTime:any;    
}