export class BatchModel {
    id: number;
    programId: number;
    batchCode: string;
    batchName: string;
    endDate: any;
    startDate: any;
}