export class Topic{

    topicId: number;

    topicTitle: string;

    topicDescription: string;
}
