export class Task{

  taskId: number;

  taskStatus: number;

  taskTitle: string;

  topicId: number;

  taskDescription: string;
}
