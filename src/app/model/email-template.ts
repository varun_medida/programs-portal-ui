export class EmailTemplate {
  templateId: number;
  templateName: string;
  subject: string;
  status: string;
}
