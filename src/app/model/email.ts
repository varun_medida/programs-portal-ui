export class Email {
  template: string;
  batchTopicId: number;
  batchId: number;
  participantStatus: string;
  emailType: string;
}
