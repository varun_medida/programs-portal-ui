import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApphttpclientService {

  constructor(private http: HttpClient) { }

  private programsportal = "http://10.71.12.47:9090/";

  private webinarUrl="http://10.71.12.47:8080/";

  private urlTemplate = "http://10.71.12.47:9091/";

  // Programs Portal Methods

  public get(endpoint){
    return this.http.get("/"+endpoint);
   }

  public post(endpoint,body){
    return this.http.post("/"+endpoint,body);
  }

  put(endpoint, body){
    return this.http.put("/"+endpoint, body);
  }

  public delete(endpoint){
    return this.http.delete("/"+endpoint);
  }

  // Webinar Service Methods
  
  public postWebinar(endpoint,body){
    return this.http.post("/"+endpoint,body);
  }

  public getWebinar(endpoint){
    return this.http.get("/"+endpoint);
  }

  public putWebinar(endpoint,body){
    return this.http.put("/"+endpoint,body);
  }

  // Notifications Service Methods

  public getTemplate(endpoint){
    return this.http.get("/"+endpoint);
  }

  public postEmail(endpoint,body)
  {
    return this.http.post("/"+endpoint,body);
  }

}
