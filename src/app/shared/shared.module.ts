import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideNavComponent } from './side-nav/side-nav.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { SharedRoutingModule } from './shared-routing.module';



@NgModule({
  declarations: [SideNavComponent, NavBarComponent],
  imports: [
    CommonModule,
    SharedRoutingModule
  ],
  exports:[SideNavComponent, NavBarComponent]
})
export class SharedModule { }
